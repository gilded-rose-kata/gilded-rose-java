# Development Setup

In an attempt to make this process smooth and easy for a variety of languages and platforms, we are using [Development Containers](https://containers.dev/).

This means you will be working in Visual Studio Code inside a pre-configured container.

## Install Visual Studio Code, Docker, and the Dev Containers Extension

Follow the instructions in the [Dev Containers Tutorial](https://code.visualstudio.com/docs/devcontainers/tutorial), stopping just before the [Get the sample](https://code.visualstudio.com/docs/devcontainers/tutorial#_get-the-sample) section.

## Open the Project in the Container

File > Open Folder (Choose this folder)

The IDE will prompt you to "Reopen in Container". If that notification is not available, click on the green box in the lower right corner of the IDE and choose to "Reopen in Container".

Do not install any further extensions unless you know what you're doing. The container has already been setup for development.

## Sharing Git SSH Key

If using an SSH key to clone/push, you may need to share that SSH key with the devcontainer.

On MacOS, you should just have to add your ssh key to the local SSH agent with `ssh-add <key>`, typically `ssh-add $HOME/.ssh/id_rsa`.

From then on, your ssh key should be forwarded into the devcontainer.

For more information, see [Sharing Git credentials with your container](https://code.visualstudio.com/remote/advancedcontainers/sharing-git-credentials).